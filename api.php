<?php
// ini_set('display_errors', 'On');
  if(count($_POST) > 0){
    if($_POST['app'] === "libro"){
      header("Content-Type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=libro.xlsx");

      $f = explode("-",$_POST['fecha']);
      $ff = $f[0] . "/" . $f[1] . "/" . $f[2];

      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $ff,
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/descargaLibro.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      echo $data;
    }
    else if($_POST['app'] === "eliminarMarca"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut'],
        'almuerzo' => $_POST['almuerzo'],
        'tipo' => $_POST['tipo']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/eliminarMarca.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      $json = json_decode($data);

      if($json->mensaje == "Logrado"){
        echo "correcto";
      }
      else{
        echo "error";
      }
    }
    else if($_POST['app'] === "ingresaHHEE"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut'],
        'minAntes' => $_POST['minAntes'],
        'porAntes' => $_POST['porAntes'],
        'minDespues' => $_POST['minDespues'],
        'porDespues' => $_POST['porDespues']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/ingresoHoraExtra.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      if($data == "true"){
        echo "correcto";
      }
      else{
        echo "error";
      }
    }
    else if($_POST['app'] === "justificaIngreso"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut'],
        'tiempoJustifica' => $_POST['tiempoJustifica']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/justificacionIngreso.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      $json = json_decode($data);

      if($json->result == "true"){
        echo "correcto";
      }
      else{
        echo "error";
      }
    }
    else if($_POST['app'] === "justificaSalida"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut'],
        'tiempoJustifica' => $_POST['tiempoJustifica']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/justificacionSalida.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      $json = json_decode($data);

      if($json->result == "true"){
        echo "correcto";
      }
      else{
        echo "error";
      }
    }
    else if($_POST['app'] === "marcaManual"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut'],
        'tipo'=> $_POST['tipo'],
        'fechaHora' => $_POST['fechaHora']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/ingresoMarcaManual.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      $json = json_decode($data);

      if($json->result == "true"){
        echo "correcto";
      }
      else{
        echo "error";
      }
    }
    else if($_POST['app'] === "moverPosible"){
      header('Content-type: application/json');
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/moverPosibleMarca.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      echo $data;
    }
    else if($_POST['app'] === "moverMarcaIngreso"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut'],
        'dondeMover' => $_POST['dondeMover'],
        'fechaMover' => $_POST['fechaMover']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/moverMarcaIngreso.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      $json = json_decode($data);

      if($json->result == "true"){
        echo "correcto";
      }
      else{
        echo "error";
      }
    }
    else if($_POST['app'] === "moverMarcaSalida"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut'],
        'dondeMover' => $_POST['dondeMover'],
        'fechaMover' => $_POST['fechaMover']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/moverMarcaSalida.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      $json = json_decode($data);

      if($json->result == "true"){
        echo "correcto";
      }
      else{
        echo "error";
      }
    }
    else if($_POST['app'] === "ingresaPermiso"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'rut' => $_POST['rut'],
        'fechaInicio' => $_POST['fechaInicio'],
        'fechaTermino' => $_POST['fechaTermino'],
        'idPermiso' => $_POST['idPermiso']
      );

      $url = "https://demo-e-gestiontech.cl/geo_beta/ingresaPermiso.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      $json = json_decode($data);

      if($json->result == "true"){
        echo "correcto";
      }
      else{
        echo "error";
      }
    }
    else if($_POST['app'] === "informeAsistPDF"){
      $post = array(
        'idEmpresa' => $_POST['idEmpresa'],
        'fecha' => $_POST['fecha'],
        'usuario' => $_POST['usuario'],
        'clave' => $_POST['clave'],
        'fechaInicio' => $_POST['fechaInicio'],
        'fechaTermino' => $_POST['fechaTermino']
      );

      for($k = 7; $k < count($_POST); $k++){
        $post[$k - 6] = $_POST[$k - 6];
      }

      $url = "https://demo-e-gestiontech.cl/geo_beta/generaInformeAsistPDF.php";

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      curl_close($ch);

      echo $data;
    }
  }
  else{
    echo "Sin datos Post";
  }
?>
