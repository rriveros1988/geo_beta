<?php
	header('Access-Control-Allow-Origin: *');
	header("Content-Type: text/html;charset=utf-8");

	//Inserta hora consumo
	function insertaFechaHoraHHEE(){
		require('conexion.php');
		$params = array(&$_POST['query']);  
		$tsql = "INSERT INTO GEOVICTORIA_HHEE_FECHAHORA_CARGA(FECHAHORA)
VALUES(GETDATE())";

		sqlsrv_begin_transaction($conn);
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);  
		$getProducts = sqlsrv_query($conn, $tsql, $params, $cursorType);  
		if ( $getProducts === false)
		{
			$ddf = fopen('error.log','a');
			fwrite($ddf, "\r\n\r\n" . date("d/m/Y") . ", Error: " . sqlsrv_errors()[0][2]);
			fclose($ddf);
			sqlsrv_rollback($conn); 
		}
		else{
			sqlsrv_commit($conn);
			return "correcto";
		}
	}
	
	//Insertar patente
	function normalizaCarga($dia){
		echo $dia . "<br/>";
		require('conexion.php');
		$params = array(&$_POST['query']);  
		$tsql = "INSERT INTO OPERACIONES.dbo.GEOVICTORIA_HHEE SELECT P.*, CAST('" . $dia . "' AS DATE), '23:30:00' FROM OPERACIONES.dbo.GEOVICTORIA_TEMP_HHEE P";
		echo $tsql . "<br/>";
		sqlsrv_begin_transaction($conn);
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);  
		$getProducts = sqlsrv_query($conn, $tsql, $params, $cursorType);  
		if ( $getProducts === false)
		{
			$ddf = fopen('error.log','a');
			fwrite($ddf, "\r\n\r\n" . date("d/m/Y") . ", Error: " . sqlsrv_errors()[0][2]);
			fclose($ddf);
			sqlsrv_rollback($conn); 
		}
		else{
			sqlsrv_commit($conn);
			return "correcto";
		}
	}

	function delCarga($dia){
		echo $dia . "<br/>";
		require('conexion.php');
		$params = array(&$_POST['query']);  
		$tsql = "DELETE GEOVICTORIA_HHEE
WHERE fecha_carga = '" . $dia . "'
AND hora_carga = '23:30:00'";
		echo $tsql . "<br/>";
		sqlsrv_begin_transaction($conn);
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);  
		$getProducts = sqlsrv_query($conn, $tsql, $params, $cursorType);  
		if ( $getProducts === false)
		{
			$ddf = fopen('error.log','a');
			fwrite($ddf, "\r\n\r\n" . date("d/m/Y") . ", Error: " . sqlsrv_errors()[0][2]);
			fclose($ddf);
			sqlsrv_rollback($conn); 
		}
		else{
			sqlsrv_commit($conn);
			return "correcto";
		}
	}

	function delCargaPor($dia){
		echo $dia . "<br/>";
		require('conexion.php');
		$params = array(&$_POST['query']);  
		$tsql = "DELETE GEOVICTORIA_HHEE_PORCENTAJE
WHERE fecha_carga = '" . $dia . "'";
		echo $tsql . "<br/>";
		sqlsrv_begin_transaction($conn);
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);  
		$getProducts = sqlsrv_query($conn, $tsql, $params, $cursorType);  
		if ( $getProducts === false)
		{
			$ddf = fopen('error.log','a');
			fwrite($ddf, "\r\n\r\n" . date("d/m/Y") . ", Error: " . sqlsrv_errors()[0][2]);
			fclose($ddf);
			sqlsrv_rollback($conn); 
		}
		else{
			sqlsrv_commit($conn);
			return "correcto";
		}
	}

	function normalizaCargaPor($dia){
		echo $dia . "<br/>";
		require('conexion.php');
		$params = array(&$_POST['query']);  
		$tsql = "INSERT INTO OPERACIONES.dbo.GEOVICTORIA_HHEE_PORCENTAJE 
		SELECT P.NOMBRE, P.DNI, P.HHEE50, P.HHEE60, P.HHEE85, P.HHEE100, P.HHEE120, CAST('" . $dia . "' AS DATE) 
		FROM OPERACIONES.dbo.GEOVICTORIA_HHEE_PORCENTAJE_TEMP P";
		echo $tsql . "<br/>";
		sqlsrv_begin_transaction($conn);
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);  
		$getProducts = sqlsrv_query($conn, $tsql, $params, $cursorType);  
		if ( $getProducts === false)
		{
			$ddf = fopen('error.log','a');
			fwrite($ddf, "\r\n\r\n" . date("d/m/Y") . ", Error: " . sqlsrv_errors()[0][2]);
			fclose($ddf);
			sqlsrv_rollback($conn); 
		}
		else{
			sqlsrv_commit($conn);
			return "correcto";
		}
	}

	function bloquesTurno($idTurno, $idEmpresa){
		require('conexion.php');
		$params = array(&$_POST['query']);  
		$tsql = "EXEC dbo.GEOVICTORIA_BETA_BLOQUES_TURNO '{$idEmpresa}','{$idTurno}'";  
	
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);  
		$getProducts = sqlsrv_query($conn, $tsql);  
		if ( $getProducts === false)  
		{
			return "Error";
		}  
		else if(sqlsrv_has_rows($getProducts))  
		{   
			$array = array(array());

			$i = 0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))  
			{
				$array[$i] = $row;
				$i++;
			} 
			return $array;
		}
	}
?>