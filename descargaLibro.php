<?php
    //header('Content-Type: text/plain; charset=utf-8');
    // ini_set('display_errors', 'Off');
    if(count($_POST) > 0){
      $url = 'http://beta.geovictoria.com/account/login';

      // $cookie="C:\\xampp\\htdocs\\Git\\geo_beta\\cookieDL.txt";
      $cookie="/home/rriveros/public_html/geo_beta/cookieDL.txt";

      if(!file_exists($cookie)) {
          $fh = fopen($cookie, "w");
          fwrite($fh, "");
          fclose($fh);
      }

      $post = array(
        'ReturnUrl' => '',
        'usuario'=> $_POST['usuario'],
        'password'=> $_POST['clave']
      );

      $request = array();
      $request[] = 'POST /account/login HTTP/1.1';
      $request[] = 'Host: beta.geovictoria.com';
      $request[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0';
      $request[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
      $request[] = 'Accept-Language: es-CL,es;q=0.8,en-US;q=0.5,en;q=0.3';
      $request[] = 'Accept-Encoding: gzip, deflate';
      $request[] = 'Content-Type: application/x-www-form-urlencoded';
      $request[] = 'Content-Length: 48';
      $request[] = 'Origin: http://beta.geovictoria.com';
      $request[] = 'DNT: 1';
      $request[] = 'Connection: keep-alive';
      $request[] = 'Referer: http://beta.geovictoria.com/account/login';
      $request[] = 'Cookie: REDIRECCIONAR_LOGIN_RAET=False; VicSessionCookie=oauqgpgegxcxfx5c3wvl3xg2';
      $request[] = 'Upgrade-Insecure-Requests: 1';
      $request[] = 'Pragma: no-cache';
      $request[] = 'Cache-Control: no-cache';

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      // curl_setopt($ch, CURLOPT_HTTPHEADER, $request);
      curl_setopt($ch, CURLOPT_ENCODING,"");


      $data = curl_exec($ch);

      // $print = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\ch1.html", "w+");
      // fwrite($print, $data);
      // fclose($print);

      if (curl_errno($ch)){
          echo "Error";
          curl_close($ch);
      }
      else{
        curl_close($ch);
        $url2 = 'http://beta.geovictoria.com/user/cambiarempresa';

        $post2 = array(
            'idEmpresa' => $_POST['idEmpresa']
        );

        $ch2 = curl_init($url2);

        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch2, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch2, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $post2);
        // curl_setopt($ch2, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch2, CURLOPT_ENCODING,"");

        $data2 = curl_exec($ch2);

        curl_close($ch2);

        // var_dump($data2);


        /* -------------------------------------------------------------*/
        $random = generateRandomString();

        $url3 = 'http://beta.geovictoria.com/gvplanning/reportefileexcel';

        $fecha = date('Ymdhis');

        $random = $random . $fecha;

        $post3 = array(
          'pagina ' => '1',
          'idReportePersonalizado' =>  '',
          'guidReporteSolicitado' => $random,
          'tipoFormatoReporte' =>  'Excel',
          'nombreReporte' => 'reporteNuevoVirtualAsistencia',
          'identificadorReporte' => '                                        Gestión+de+Asistencia                ',
          'Fecha_Raw' => $_POST['fecha'] . ' - ' . $_POST['fecha'],
          'Semana_Raw' =>  '',
          'Fecha_Apertura_Raw' =>  '',
          'NotificacionesVistas' =>  '2',
          'TiposDeNotificaciones' => '6',
          'TiposDeIntentoMarcaje' => '3',
          'TiposDeSNCasino' => '1',
          'TiposDeMarcasFueraDeGrupo' => '3',
          'BloqueoEnvioCorreo' =>  '1',
          'ReporteDomingos' => '1',
          'Personas_Raw' =>  'Todos',
          'objetos_raw' => 'Todos',
          'SelectorPersonas' =>  'Todos',
          'direccion_orden' => 'asc',
          'campo_a_ordenar' => 'APELLIDO',
          'verUsuariosCasino' => 'todo',
          'pdfHorizontal' => 'true',
          'tipo_ordenamiento' => 'Grupo',
          'direccion' => 'asc',
          'SelectorGruposAll' => 'Todos',
          'Grupos_RawAll' => 'Todos',
          'TipoSolicitud' => '0',
          'IncluirDesactivados' => '',
          'Proyectos_Raw' => 'Todos',
          'TipoPermisos_Raw' =>  'Todos',
          'SelectorGrupos' =>  'Todos',
          'Grupos_Raw' => 'Todos'
        );

        $ch3 = curl_init($url3);

        curl_setopt($ch3, CURLOPT_HEADER, 0);
        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch3, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch3, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch3, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch3, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch3, CURLOPT_POST, 1);
        curl_setopt($ch3, CURLOPT_POSTFIELDS, $post3);
        // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch3, CURLOPT_ENCODING,"");

        $data3 = curl_exec($ch3);

        curl_close($ch3);

        // $print = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\ch3.html", "w+");
        // fwrite($print, $data3);
        // fclose($print);

        // var_dump($data3);

        $respuesta = json_decode($data3);

        // var_dump($respuesta);

        if($respuesta == "ok"){
            $check = '';
            while($check == ''){
              $url4 = 'http://beta.geovictoria.com/reports/export/comprobarestadoreporteextensoonline';

              $post4 = array(
                  'guid' => $random
              );

              //var_dump($post4);

              $ch4 = curl_init($url4);

              curl_setopt($ch4, CURLOPT_HEADER, 0);
              curl_setopt($ch4, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, 0);
              curl_setopt($ch4, CURLOPT_COOKIEFILE, $cookie);
              curl_setopt($ch4, CURLOPT_COOKIEJAR, $cookie);
              curl_setopt($ch4, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
              curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, 1);
              curl_setopt($ch4, CURLOPT_POST, 1);
              curl_setopt($ch4, CURLOPT_POSTFIELDS, $post4);
              // curl_setopt($ch4, CURLOPT_HTTPHEADER, $request);
              curl_setopt($ch4, CURLOPT_ENCODING,"");

              $data4 = curl_exec($ch4);
              $array = (array)json_decode($data4);

              curl_close($ch4);

              // var_dump($array);

              $check = $array['url'];

              //echo $check;

              if($check != ''){
                $url = $check;

                // $descarga = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\descargas\\geo_" . $_POST['idEmpresa'] . ".xlsx", "w+");

                $ch5 = curl_init($url);

                curl_setopt($ch5, CURLOPT_HEADER, 0);
                curl_setopt($ch5, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch5, CURLOPT_COOKIEFILE, $cookie);
                curl_setopt($ch5, CURLOPT_COOKIEJAR, $cookie);
                curl_setopt($ch5, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                curl_setopt($ch5, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch5, CURLOPT_POST, 0);
                // curl_setopt($ch5, CURLOPT_FILE, $descarga);

                $download = curl_exec($ch5);

                // fclose($descarga);

                echo $download;

                curl_close($ch5);

                break;
              }
            }
        }
        else{
            echo "Error generando reporte excel";
        }
      }

        /*
        $print = fopen("C:\\wamp64\\www\\corp\\geo\\print.html", "w+");
        fwrite($print, $data);
        fclose($print);
        */

        $url = 'http://beta.geovictoria.com/account/login';

        $ch5 = curl_init($url);

        curl_setopt($ch5, CURLOPT_HEADER, 0);
        curl_setopt($ch5, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch5, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch5, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch5, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch5, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch5, CURLOPT_POST, 0);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch5, CURLOPT_ENCODING,"");


        $data5 = curl_exec($ch5);

        // $print = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\ch5.html", "w+");
        // fwrite($print, $data5);
        // fclose($print);
    }
    else{
      echo "Sin datos Post";
    }

    function generateRandomString($length = 22) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    unlink("/home/rriveros/public_html/geo_beta/cookieDL.txt");
?>
