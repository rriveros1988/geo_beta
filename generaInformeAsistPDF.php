<?php
    //header('Content-Type: text/plain; charset=utf-8');
    //ini_set('display_errors', 'On');
    if(count($_POST) > 0){
      $url = 'http://beta.geovictoria.com/account/login';

      // $cookie="C:\\xampp\\htdocs\\Git\\geo_beta\\cookieHE.txt";
      $cookie="/home/rriveros/public_html/geo_beta/cookieHE.txt";

      if(!file_exists($cookie)) {
          $fh = fopen($cookie, "w");
          fwrite($fh, "");
          fclose($fh);
      }

      $post = array(
        'ReturnUrl' => '',
        'usuario'=> $_POST['usuario'],
        'password'=> $_POST['clave']
      );

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      // curl_setopt($ch, CURLOPT_HTTPHEADER, $request);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      // $print = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\ch1.html", "w+");
      // fwrite($print, $data);
      // fclose($print);

      if (curl_errno($ch)){
          echo "Error";
          curl_close($ch);
      }
      else{
        curl_close($ch);
        $url2 = 'http://beta.geovictoria.com/user/cambiarempresa';

        $post2 = array(
            'idEmpresa' => $_POST['idEmpresa']
        );

        $ch2 = curl_init($url2);

        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch2, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch2, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $post2);
        // curl_setopt($ch2, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch2, CURLOPT_ENCODING,"");

        $data2 = curl_exec($ch2);

        curl_close($ch2);

        /* -------------------------------------------------------------*/
        $f = explode("-",$_POST['fechaInicio']);
        $ff = $f[0] . "/" . $f[1] . "/" . $f[2];
        $f2 = explode("-",$_POST['fechaTermino']);
        $ft = $f2[0] . "/" . $f2[1] . "/" . $f2[2];

        $random = generateRandomString();

        $fecha = date('Ymdhis');

        $random = $random . $fecha;

        $per = '';
        $u_per = '';

        for($k = 6; $k < count($_POST); $k++){
          if($k == 6){
            $per = $_POST[$k-5];
          }
          else{
            $per = $per . ',' . $_POST[$k-5];
          }

          if($k == 6){
            $u_per = 'u_' . $_POST[$k-5];
          }
          else{
            $u_per = $u_per . ',u_' . $_POST[$k-5];
          }
        }

        $url4 = 'http://beta.geovictoria.com/gvplanning/reportefilepdf';

        $post4 = array(
          'pagina' =>	"1",
          'idReportePersonalizado' =>	"",
          'guidReporteSolicitado' =>	$random,
          'tipoFormatoReporte' =>	"pdf",
          'nombreReporte' =>	"reporteNuevoVirtualAsistencia",
          'identificadorReporte' =>	"\r\n++++++++\r\n++++++++++++++++\r\n++++++++\r\n++++++++Gestión+de+Asistencia\r\n++++\r\n\r\n++++++++\r\n++++",
          'Fecha_Raw' =>	$ff . " - " . $ft,
          'Semana_Raw' =>	"",
          'Fecha_Apertura_Raw' =>	"",
          'NotificacionesVistas' =>	"2",
          'TiposDeNotificaciones' =>	"6",
          'TiposDeIntentoMarcaje' =>	"3",
          'TiposDeSNCasino' =>	"1",
          'TiposDeMarcasFueraDeGrupo' =>	"3",
          'BloqueoEnvioCorreo' =>	"1",
          'ReporteDomingos' =>	"1",
          'Personas_Raw' =>	$per,
          'objetos_raw' =>	$u_per,
          'SelectorPersonas' =>	"selecciona",
          'direccion_orden' =>	"asc",
          'campo_a_ordenar' =>	"APELLIDO",
          'verUsuariosCasino' =>	"todo",
          'pdfHorizontal' =>	"true",
          'tipo_ordenamiento' =>	"Grupo",
          'direccion' =>	"asc",
          'SelectorGruposAll' =>	"Todos",
          'Grupos_RawAll' =>	"Todos",
          'TipoSolicitud' =>	"0",
          'IncluirDesactivados' =>	"false",
          'Proyectos_Raw' =>	"Todos",
          'TipoPermisos_Raw' =>	"Todos",
          'SelectorGrupos' =>	"Todos",
          'Grupos_Raw' =>	"Todos"
        );

        $ch4 = curl_init($url4);

        curl_setopt($ch4, CURLOPT_HEADER, 0);
        curl_setopt($ch4, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch4, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch4, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch4, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch4, CURLOPT_POST, 1);
        curl_setopt($ch4, CURLOPT_POSTFIELDS, $post4);
        // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch4, CURLOPT_ENCODING,"");

        $data4 = curl_exec($ch4);

        curl_close($ch4);

        // echo "<pre>";
        // var_dump($data4);
        // echo "</pre>";

        if($data4 == '"ok"'){
          $a = 0;
          while($a === 0){
            $url5 = 'http://beta.geovictoria.com/reports/export/comprobarestadoreporteextensoonline';

            $post5 = array(
              'guid' => $random
            );

            $ch5 = curl_init($url5);

            curl_setopt($ch5, CURLOPT_HEADER, 0);
            curl_setopt($ch5, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch5, CURLOPT_COOKIEFILE, $cookie);
            curl_setopt($ch5, CURLOPT_COOKIEJAR, $cookie);
            curl_setopt($ch5, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
            curl_setopt($ch5, CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($ch5, CURLOPT_POST, 1);
            curl_setopt($ch5, CURLOPT_POSTFIELDS, $post5);
            // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
            curl_setopt($ch5, CURLOPT_ENCODING,"");

            $data5 = curl_exec($ch5);

            curl_close($ch5);

            $jdata5 = json_decode($data5);
            $jdata5 = (array)$jdata5;
            if($jdata5['completado'] === true){
              $a = 1;
              echo $jdata5['url'];
            }
          }
        }

      }
    }
    else{
      echo "Sin datos Get";
    }

    function generateRandomString($length = 22) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    unlink("C:/xampp/htdocs/Git/geo_beta/cookieHE.txt");
?>
