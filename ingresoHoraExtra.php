<?php
    //header('Content-Type: text/plain; charset=utf-8');
    //ini_set('display_errors', 'On');
    if(count($_POST) > 0){
      $url = 'http://beta.geovictoria.com/account/login';

      // $cookie="C:\\xampp\\htdocs\\Git\\geo_beta\\cookieHE.txt";
      $cookie="/home/rriveros/public_html/geo_beta/cookieHE.txt";

      if(!file_exists($cookie)) {
          $fh = fopen($cookie, "w");
          fwrite($fh, "");
          fclose($fh);
      }

      $post = array(
        'ReturnUrl' => '',
        'usuario'=> $_POST['usuario'],
        'password'=> $_POST['clave']
      );

      $ch = curl_init($url);

      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      // curl_setopt($ch, CURLOPT_HTTPHEADER, $request);
      curl_setopt($ch, CURLOPT_ENCODING,"");

      $data = curl_exec($ch);

      // $print = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\ch1.html", "w+");
      // fwrite($print, $data);
      // fclose($print);

      if (curl_errno($ch)){
          echo "Error";
          curl_close($ch);
      }
      else{
        curl_close($ch);
        $url2 = 'http://beta.geovictoria.com/user/cambiarempresa';

        $post2 = array(
            'idEmpresa' => $_POST['idEmpresa']
        );

        $ch2 = curl_init($url2);

        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch2, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch2, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $post2);
        // curl_setopt($ch2, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch2, CURLOPT_ENCODING,"");

        $data2 = curl_exec($ch2);

        curl_close($ch2);

        /*---------------------------*/
        $url3 = 'http://beta.geovictoria.com/user/completeuserslist';

        $post3 = array(
          'ignoreIds' => '',
          'searchText' =>  $_POST['rut']
        );

        $ch3 = curl_init($url3);

        curl_setopt($ch3, CURLOPT_HEADER, 0);
        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch3, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch3, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch3, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch3, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch3, CURLOPT_POST, 1);
        curl_setopt($ch3, CURLOPT_POSTFIELDS, $post3);
        // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch3, CURLOPT_ENCODING,"");

        $data3 = curl_exec($ch3);

        $datosUsuario = json_decode($data3);
        $id = '';

        for($i = 0; $i < count($datosUsuario); $i++){
          $arrayDatosUsuario = (array)$datosUsuario[$i];
          if($arrayDatosUsuario['HABILITADO'] == '1'){
            $id = $arrayDatosUsuario['ID_USUARIO'];
          }
        }

        // echo "id: " . $id . "<br>";

        /* -------------------------------------------------------------*/
        $f = explode("-",$_POST['fecha']);
        $ff = $f[0] . "/" . $f[1] . "/" . $f[2];

        $url4 = 'http://beta.geovictoria.com/gvplanning/reporteconfig';

        $post4 = array(
          'pagina' =>	"1",
          'idReportePersonalizado' =>	"",
          'guidReporteSolicitado' =>	"",
          'tipoFormatoReporte' =>	"",
          'nombreReporte' =>	"reporteNuevoVirtualAsistencia",
          'identificadorReporte' =>	"\r\n++++++++\r\n++++++++++++++++\r\n++++++++\r\n++++++++Gestión+de+Asistencia\r\n++++\r\n\r\n++++++++\r\n++++",
          'Fecha_Raw' =>	$ff . " - " . $ff,
          'Semana_Raw' =>	"",
          'Fecha_Apertura_Raw' =>	"",
          'NotificacionesVistas' =>	"2",
          'TiposDeNotificaciones' =>	"6",
          'TiposDeIntentoMarcaje' =>	"3",
          'TiposDeSNCasino' =>	"1",
          'TiposDeMarcasFueraDeGrupo' =>	"3",
          'BloqueoEnvioCorreo' =>	"1",
          'ReporteDomingos' =>	"1",
          'Personas_Raw' =>	$id,
          'objetos_raw' =>	"u_" . $id,
          'SelectorPersonas' =>	"selecciona",
          'direccion_orden' =>	"asc",
          'campo_a_ordenar' =>	"APELLIDO",
          'verUsuariosCasino' =>	"todo",
          'pdfHorizontal' =>	"true",
          'tipo_ordenamiento' =>	"Grupo",
          'direccion' =>	"asc",
          'SelectorGruposAll' =>	"Todos",
          'Grupos_RawAll' =>	"Todos",
          'TipoSolicitud' =>	"0",
          'IncluirDesactivados' =>	"false",
          'Proyectos_Raw' =>	"Todos",
          'TipoPermisos_Raw' =>	"Todos",
          'SelectorGrupos' =>	"Todos",
          'Grupos_Raw' =>	"Todos"
        );

        $ch4 = curl_init($url4);

        curl_setopt($ch4, CURLOPT_HEADER, 0);
        curl_setopt($ch4, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch4, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch4, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch4, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch4, CURLOPT_POST, 1);
        curl_setopt($ch4, CURLOPT_POSTFIELDS, $post4);
        // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch4, CURLOPT_ENCODING,"");

        $data4 = curl_exec($ch4);

        curl_close($ch4);

        // echo "<pre>";
        // var_dump($post4);
        // echo "</pre>";

        $htmlDom = new DOMDocument;
        $htmlDom->loadHTML($data4);
        foreach ($htmlDom->getElementsByTagName('input') as $tag) {
          if($tag->getAttribute('id') === 'dataJSONPlanning'){
            $datoJson = $tag->getAttribute("value");
          }
          if($tag->getAttribute('name') === 'vsk'){
            $vsk = $tag->getAttribute("value");
          }
        }

        // echo "vsk: " . $vsk . "<br>";
        // echo "Json: " . $datoJson . "<br>";

        /* ------------------------------- */
        $url5 = "https://gvplanning.geovictoria.com/ModuleReport/WeeklyAttendance";

        $post5 = array(
          'vsk' => $vsk,
          'dataJSON' => $datoJson
        );

        $ch5 = curl_init($url5);

        curl_setopt($ch5, CURLOPT_HEADER, 0);
        curl_setopt($ch5, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch5, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch5, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch5, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch5, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch5, CURLOPT_POST, 1);
        curl_setopt($ch5, CURLOPT_POSTFIELDS, $post5);
        // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch5, CURLOPT_ENCODING,"");

        $data5 = curl_exec($ch5);

        curl_close($ch5);

        // $print = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\ch5.html", "w+");
        // fwrite($print, $data5);
        // fclose($print);

        /* --------------------------------------- */
        $f = explode("-",$_POST['fecha']);
        $ff = $f[2] . $f[1] . $f[0];

        $url6 = "https://gvplanning.geovictoria.com/ModuleCommons/OverTimeInteraction/Edit";

        $post6 = array(
          'idUsuario' => $id,
          'fecha'	=> $ff,
          'nroSecuencia' =>	"2"
        );

        $ch6 = curl_init($url6);

        curl_setopt($ch6, CURLOPT_HEADER, 0);
        curl_setopt($ch6, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch6, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch6, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch6, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch6, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch6, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch6, CURLOPT_POST, 1);
        curl_setopt($ch6, CURLOPT_POSTFIELDS, $post6);
        // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch6, CURLOPT_ENCODING,"");

        $data6= curl_exec($ch6);

        curl_close($ch6);

        // $print = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\ch6.html", "w+");
        // fwrite($print, $data6);
        // fclose($print);

        /* --------------------------------------------- */
        $f = explode("-",$_POST['fecha']);
        $ff = $f[2] . "-" . $f[1] . "-" . $f[0];

        $url7 = "https://gvplanning.geovictoria.com/ModuleCommons/OverTimeInteraction/Save";

        $post7 = array(
          'horaExtra[IdUsuario]' => $id,
          'horaExtra[FechaInicioHE]' => $ff . " 00:00:00",
          'horaExtra[FechaFinHE]' => $ff . " 23:59:59",
          'horaExtra[DuracionAntesString]' => $_POST['minAntes'],
          'horaExtra[ValorHoraExtraAntes]' => $_POST['porAntes'],
          'horaExtra[DuracionHEString]' => $_POST['minDespues'],
          'horaExtra[ValorHoraExtra]' => $_POST['porDespues'],
          'horaExtra[NroSecuencia]' => "2",
          'horaExtra[IdTipoMotivoHoraExtra]' => "-1",
          'horaExtra[ComentarioMotivoHoraExtra]' => ""
        );

        $ch7 = curl_init($url7);

        curl_setopt($ch7, CURLOPT_HEADER, 0);
        curl_setopt($ch7, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch7, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch7, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch7, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch7, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        curl_setopt($ch7, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch7, CURLOPT_POST, 1);
        curl_setopt($ch7, CURLOPT_POSTFIELDS, $post7);
        // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
        curl_setopt($ch7, CURLOPT_ENCODING,"");

        $data7 = curl_exec($ch7);

        curl_close($ch7);

        // $print = fopen("C:\\xampp\\htdocs\\Git\\geo_beta\\ch7.html", "w+");
        // fwrite($print, $data7);
        // fclose($print);

        echo $data7;
      }
    }
    else{
      echo "Sin datos Get";
    }

    function generateRandomString($length = 22) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    unlink("/home/rriveros/public_html/geo_beta/cookieHE.txt");
?>
